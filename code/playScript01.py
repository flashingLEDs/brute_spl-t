"""
playScript01.py

Usage:
------
python playScript01.py -boardwidth -boardHeight

Description:
------------
Simulates games of SPL-T starting from a blank board, with randomly chosen splits.
Runs parallel instances on all CPU cores.


Hard coded parameters:
	gamesPerBatch	How many games to play before saving to file
	numBatches		How many batches to run

	(Adjust these to make disk writes more or less frequent)

Output:
-------
Generates 2 output files per instance:

[playScript01#]rXc.txt
		Length and score for all games simulated

[playScript01#]rXc_bestSequences.txt
		Score, length and move sequence for any sequence longer than the current maximum of this instance.

(#=instance label (a,b,c,d,e,f,g...), rXc = boardWidth x boardHeight):

If these output files already exist, they are appended to rather than overwritten.

"""

import sys 
import os
import core
import multiprocessing
import random


gamesPerBatch=100
numBatches=1000000

##########################################
def playSplitRandomly(gameBoard):
	#Play a single game of SPL-T
##########################################	
	while 1:

		moveOptions=gameBoard.getMoveOptions()

		if len(moveOptions)==0:
			return gameBoard.score,gameBoard.splitRecord
			break

		nextMove=random.choice(moveOptions)


		core.makeMove(gameBoard,nextMove)


##########################################
def PlayGames(boardWidth,boardHeight,saveLabel,):
	# Play many games of SPL-T
##########################################

	saveFile_1_Name = "[playScript01"+saveLabel+"]{0}x{1}.txt".format(boardWidth,boardHeight)
	saveFile_2_Name = "[playScript01"+saveLabel+"]{0}x{1}_bestSequences.txt".format(boardWidth,boardHeight)
	
	if not os.path.isfile(saveFile_1_Name):
		saveFile=open(saveFile_1_Name,'w')
		saveFile.write("Score\tLength\n")
		saveFile.close()
	if not os.path.isfile(saveFile_2_Name):
		saveFile=open(saveFile_2_Name,'w')
		saveFile.write("Score\tLength\tPath\n")
		saveFile.close()

	
	#For each game played, keep a record of:
	paths=[]
	pathLengths=[]	
	scores=[]

	#In addition, some meta-parameters:
	bestLength=0
	bestScore=0
	bestSequence=[]

	for batch in range(numBatches):
		for games in range(gamesPerBatch):

			# Initialize the board
			gameBoard=core.Board(boardWidth,boardHeight)

			# Play
			score,path=playSplitRandomly(gameBoard)

			# Save the results to buffer
			paths.append(path)
			pathLengths.append(len(path))
			scores.append(score)

			if len(path)>bestLength:
				bestLength=len(path)
				saveFile=open(saveFile_2_Name,'a')
				saveFile.write("{0}\t{1}\t{2}\n".format(score,len(path),path))
				saveFile.close()

			
			print("playScript01"+saveLabel+","+"\t Game: ",games,"\tBatch: ",batch,"\tScore: ",score,"\tLength: ",len(path),"\tMaxLength: ",bestLength)

		
		# After each batch, save the buffers to file them clear them
		saveFile=open(saveFile_1_Name,'a')
		for index,path in enumerate(paths):
			saveFile.write("{0}\t{1}\n".format(scores[index],pathLengths[index]))
		saveFile.close()

		paths=[]
		scores=[]	
		pathLengths=[]


##########################################
if __name__ == '__main__':
##########################################

	try:
		boardWidth=int(sys.argv[1])
		boardHeight=int(sys.argv[2])
	except:
		print("Didn't get the parameters I expected.\n\nExpected usage is playScript01.py <boardwidth> <boardHeight>\n")
		sys.exit(1)
	print("\n--------Random SPL-T--------\n")

	if boardWidth not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board width ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardWidth))
		sys.exit(1)
	if boardHeight not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board height ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardHeight))
		sys.exit(1)	


	numCores=multiprocessing.cpu_count()
	print("\nCPU cores available=",numCores)    

	saveLabelList=['a','b','c','d','e','f','g','h','i','j','k','l']


	### Run the function PlayGames on all available CPU cores in parallel
	jobs=[]
	for index in range(numCores):
		process = multiprocessing.Process(target=PlayGames, args=(boardWidth,boardHeight,saveLabelList[index],))
		jobs.append(process)
		process.start()

	for job in jobs:
		job.join()	

	sys.exit()
