"""
playScript04.py

Usage:
------
python playScript04.py -boardwidth -boardHeight


Description:
------------
Simulates games of spl-t starting from a blank board, with randomly chosen splits. Runs parallel instances on all CPU cores.

Output:
-------
Generates 2 output files per instance (here #=instance label (a,b,c,d,e,f,g...), rXc = boardWidth x boardHeight):

[playScript04#]rXc.txt
		Final split length and score for all games simulated

[playScript04#]_redundancyHistogram.txt
		Histogram of mirror-symmetry redundancies at each move index

If these output files already exist, they are appended to rather than overwritten.

"""

import sys 
import os
import core
import multiprocessing
import copy
import random


gamesPerBatch=1
numBatches=1000000



##########################################
def MirrorCompareBoards(gameBoard1,gameBoard2):
##########################################

	def mirrorAllowed(gameBoard,axis):
		if axis==0:
			MirrorLineX=1
		if axis==1:
			MirrorLineX=2
		if axis==2:
			MirrorLineX=4		

		for box in gameBoard.box:
			if box.x<MirrorLineX and (box.x+box.width)>MirrorLineX and ((box.x+box.width)/2) !=MirrorLineX:
				return 0
		return 1

	def mirrorBoard(gameBoard,axis):
		if axis==0:
			axisx=1
		if axis==1:
			axisx=2
		if axis==2:
			axisx=4		

		for box in gameBoard.box:
			box.x = axisx + (axisx-(box.x+box.width))
		return gameBoard

	if gameBoard1==gameBoard2:
		return 1

	if mirrorAllowed(gameBoard1,0):
		if mirrorBoard(gameBoard1,0)==gameBoard2:
			return 1

	if mirrorAllowed(gameBoard1,1):
		if mirrorBoard(gameBoard1,1)==gameBoard2:
			return 1

	if mirrorAllowed(gameBoard1,2):
		if mirrorBoard(gameBoard1,2)==gameBoard2:
			return 1

	if mirrorAllowed(gameBoard1,0) and mirrorAllowed(gameBoard1,1):
		if mirrorBoard(mirrorBoard(gameBoard1,0),1)==gameBoard2:
			return 1

	if mirrorAllowed(gameBoard1,0) and mirrorAllowed(gameBoard1,2):
		if mirrorBoard(mirrorBoard(gameBoard1,0),2)==gameBoard2:
			return 
			1
	if mirrorAllowed(gameBoard1,1) and mirrorAllowed(gameBoard1,2):
		if mirrorBoard(mirrorBoard(gameBoard1,1),2)==gameBoard2:
			return 1

	if mirrorAllowed(gameBoard1,0) and mirrorAllowed(gameBoard1,1) and mirrorAllowed(gameBoard1,2):
		if mirrorBoard(mirrorBoard(mirrorBoard(gameBoard1,0),1),2)==gameBoard2:
			return 1

	return 0

##########################################
def playSplitRandomly(gameBoard):
##########################################	
	redundancyHistory=[]
	while 1:

		moveOptions=gameBoard.getMoveOptions()

		if len(moveOptions)==0:
			return gameBoard.score,gameBoard.splitRecord,redundancyHistory
			break

		redundancyCount=0



		futureGameBoard=[]
		for index,move in enumerate(moveOptions):
			futureGameBoard.append(copy.deepcopy(gameBoard))

			core.makeMove(futureGameBoard[index],move)

		for ii in range(len(futureGameBoard)):
			for jj in range(ii+1,len(futureGameBoard)):
				if MirrorCompareBoards(futureGameBoard[ii],futureGameBoard[jj]):
					redundancyCount+=1
					#print(len(gameBoard.splitRecord),"moves in, and choices",ii,"and",jj,"will leave you with the same board")

		redundancyHistory.append(redundancyCount)

		nextMove=random.choice(moveOptions)


		core.makeMove(gameBoard,nextMove)


##########################################
def PlayGames(boardWidth,boardHeight,saveLabel,):
##########################################

	saveFile_1_Name = "[playScript04"+saveLabel+"]{0}x{1}.txt".format(boardWidth,boardHeight)

	saveFile_2_Name = "[playScript04"+saveLabel+"]{0}x{1}_redundancyHistogram.txt".format(boardWidth,boardHeight)

	if not os.path.isfile(saveFile_1_Name):
		saveFile=open(saveFile_1_Name,'w')
		saveFile.write("Score\tLength\n")
		saveFile.close()

	#For each game played, keep a record of:
	paths=[]
	pathLengths=[]	
	scores=[]


	redundancyHistogram=[]
	for ii in range(500):
		redundancyHistogram.append([0,0])

	for batch in range(numBatches):
		for games in range(gamesPerBatch):

			# Initialize the board
			gameBoard=core.Board(boardWidth,boardHeight)

			# Play
			score,path,redundancyHistory=playSplitRandomly(gameBoard)

			for index,element in enumerate(redundancyHistory):
				redundancyHistogram[index][0]+=1
				redundancyHistogram[index][1]+=element

			# Save the results to buffer
			paths.append(path)
			pathLengths.append(len(path))
			scores.append(score)
			
			print("04"+saveLabel+","+"\t Game: ",games,"\tBatch: ",batch,"\tScore: ",score,"\tLength: ",len(path))

		
		# After each batch, save the buffers to file them clear them
		saveFile=open(saveFile_1_Name,'a')
		for index,path in enumerate(paths):
			saveFile.write("{0}\t{1}\n".format(scores[index],pathLengths[index]))
		saveFile.close()

		saveFile=open(saveFile_2_Name,'w')
		saveFile.write("Index\tTimesSampled\tNumRedundancies\n")
		for index,element in enumerate(redundancyHistogram):
			saveFile.write("{0}\t{1}\t{2}\n".format(index,element[0],element[1]))
		saveFile.close()

		paths=[]
		scores=[]	
		pathLengths=[]


##########################################
if __name__ == '__main__':
##########################################

	try:
		boardWidth=int(sys.argv[1])
		boardHeight=int(sys.argv[2])
	except:
		print("\nProblem interpreting arguments\n\nExpected usage is playScript04.py <boardWidth> <boardHeight>")	
		sys.exit(1)

	if boardWidth not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board width ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardWidth))
		sys.exit(1)
	if boardHeight not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board height ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardHeight))
		sys.exit(1)	

	print("\n--------Random SPL-T--------\n")

	numCores=multiprocessing.cpu_count()
	print("\nCPU cores available=",numCores)    

	saveLabelList=['a','b','c','d','e','f','g','h','i','j','k','l']

	jobs=[]
	for index in range(numCores):
		process = multiprocessing.Process(target=PlayGames, args=(boardWidth,boardHeight,saveLabelList[index],))
		jobs.append(process)
		process.start()

	for job in jobs:
		job.join()	

	sys.exit()
