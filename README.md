# brute_spl-t

These files are for running automated, 'headless' simulations of the iOS puzzle game <i>SPL-T</i>, intended for attempts to brute force a high score. The accompanying project writeup is at <a href="http://www.flashingleds.net/brute_SPL-T/brute_spl-t.html"> http://www.flashingleds.net/brute_SPL-T/brute_spl-t.html</a>.

**code/core.py**
Other than a way of deciding which moves to make, this contains everything you need to simulate a game of <i>SPL-T</i>. The only graphical display of the board is optional ascii art printed to the command line, for debugging purposes.

**code/replay.py**
After a game simulation is finished, all you know is the list of box indices that were chosen at each turn. This is not sufficient if you want to validate the sequence by putting the same moves into a real game of <i>SPL-T</i>. Using PyQt5, this will provide a graphical representation of the gameboard as it plays through a given move sequence. At each turn it highlights what box you should choose as you play along with it.

**code/playScript_.py**
Run a specific experiment using core.py. Each experiment is explained in the writeup.

**data/**
The output from when I ran these experiments for the writeup.

**brute_spl-t.ipynb**
Jupyter notebook for plotting and simple analysis of the output data



